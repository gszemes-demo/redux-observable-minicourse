import React from 'react';
import ReactDOM from 'react-dom';
import { GithubApiSandboxApp } from './GithubApiSandboxApp';

ReactDOM.render(
  <React.StrictMode>
    <GithubApiSandboxApp />
  </React.StrictMode>,
  document.getElementById('root')
);