import {ChangeEvent, MouseEvent, useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { cancelSearch, loadGeneralInfo, loadUserData } from '../store/githubApiSandboxApp.actions';
import { AppState } from '../store/githubApiSandboxApp.reducer';

export const HomePage = () => {      
    const dispatch = useDispatch();

    const generalInfo = useSelector((state: AppState) => state.generalInfo)
    const userLoadError = useSelector((state: AppState) => state.error)      
    const isUserDataLoading = useSelector((state: AppState) => state.isUserDataLoading)        
    const searchInputValue = useSelector((state: AppState) => state.searchInputValue)    
    const userData = useSelector((state: AppState) => state.userData)

  useEffect(() => {
    console.log('app state has been changed. new input value: ' + searchInputValue)
  }, [searchInputValue])

  useEffect(() => {    
    dispatch(loadGeneralInfo())
  }, [])

  return (    
    <div >  
      <h2>Github sandbox</h2>
      <h3>Recent Activities</h3> 
      {generalInfo &&
        <>
          <div>
            <b>Latest gist:</b> <a href={generalInfo.latestGistUrl}>{generalInfo.latestGistDesc || '[no description]'}</a>
            &nbsp;(by {generalInfo.latestGistAuthor}).
          </div>          
          <div>
            <b>Latest event:</b> a <i>{generalInfo.latestEventType}</i>
            &nbsp;at repository <a href={`https://github.com/${generalInfo.latestEventRepoName}`}>{generalInfo.latestEventRepoName}</a>.
          </div>          
        </>
      }
      <h3>User finder</h3>
      <div>
        <input
            type="text"
            onChange={
                (event: ChangeEvent<HTMLInputElement>) => dispatch(loadUserData(event.target.value))
            }
        />
        <button onClick={() => dispatch(cancelSearch())}>cancel</button>
      </div>

      {userData &&
        <div style={{marginTop: 10, display: 'flex'}}>
          <img src={userData.avatar_url} width="50"/>
          <p style={{marginLeft: 10}}>
            <span>{userData.login}</span>
            {userData.name && <span>&nbsp;({userData.name})</span>}
          </p>
        </div>        
      }      
      {isUserDataLoading &&
        <div>Please wait...</div>
      }
      {userLoadError && 
        <div>User not found</div>
      }
    </div>
  );
}