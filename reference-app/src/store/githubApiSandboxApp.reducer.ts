import { AppAction, AppActionKeys } from "./githubApiSandboxApp.actions"
import { GeneralInfo } from "./githubApiSandboxApp.types"
export interface AppState {
    error?: any
    generalInfo?: GeneralInfo    
    generalInfoLoadingError?: any
    isUserDataLoading: boolean
    searchInputValue: string
    userData?: any    
}
export const initialState: AppState = {        
    isUserDataLoading: false,
    searchInputValue: "",
}
export const appReducer = (state: AppState = initialState, action: AppAction): Partial<AppState>  => {
    switch (action.type) {
        case AppActionKeys.USER_DATA_CANCEL_LOADING:
            return {         
                ...state,       
                userData: undefined,
                isUserDataLoading: false
            }
        case AppActionKeys.USER_DATA_HAS_BEEN_LOADED:
            return {
                ...state,
                userData: action.payload,
                isUserDataLoading: false
            }
        case AppActionKeys.USER_DATA_LOAD:
            return {
                ...state,
                searchInputValue: action.payload,
                isUserDataLoading: true
            }
        case AppActionKeys.USER_DATA_LOADING_ERROR:
            return {
                ...state,
                error: action.error,
                isUserDataLoading: false
            }
        case AppActionKeys.GENERAL_INFO_HAS_BEEN_LOADED:
            return {     
                ...state,           
                generalInfo: action.payload
            }
        case AppActionKeys.GENERAL_INFO_LOADING_ERROR:
            return {
                ...state,
                generalInfoLoadingError: action.error
            }
        default:
            return state
    }
}