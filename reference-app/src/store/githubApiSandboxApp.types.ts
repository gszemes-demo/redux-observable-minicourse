import { Observable, SchedulerLike } from "rxjs";
import { ajax } from "rxjs/ajax";

export interface GeneralInfo {
    latestGistAuthor: string,
    latestGistDesc: string,
    latestGistUrl: string,       
    latestEventType: string,
    latestEventRepoName: string
}
export type getJSONType = <T>(url: string) => Observable<T>
export interface Dependencies {
    //getJSON: typeof ajax.getJSON // <T>: (url: string) => Observable<T>,
    getJSON: getJSONType,
    scheduler: SchedulerLike
}