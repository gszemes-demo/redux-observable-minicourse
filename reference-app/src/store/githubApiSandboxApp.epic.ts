import { combineEpics } from "redux-observable";
import { exampleDependencyEpic } from "./epics/exampleDependencyEpic";
import { logEpic } from "./epics/logEpic";
import { queryGithubGeneralInfo } from "./epics/queryGithubGeneralInfo";
import { queryGithubUserEpic } from "./epics/queryGithubUserEpic";
export const appEpic = combineEpics(
    logEpic,
    exampleDependencyEpic,    
    queryGithubGeneralInfo,
    queryGithubUserEpic
)