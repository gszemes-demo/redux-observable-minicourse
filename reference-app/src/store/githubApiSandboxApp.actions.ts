import { Action } from "redux";
import { GeneralInfo } from "./githubApiSandboxApp.types";

export enum AppActionKeys {
    GENERAL_INFO_HAS_BEEN_LOADED =  `GENERAL_INFO_HAS_BEEN_LOADED`,
    GENERAL_INFO_LOADING_ERROR = `GENERAL_INFO_LOADING_ERROR`,
    GENERAL_INFO_LOAD = `GENERAL_INFO_LOAD`,    
    USER_DATA_LOAD = `USER_DATA_LOAD`,
    USER_DATA_HAS_BEEN_LOADED = `USER_DATA_HAS_BEEN_LOADED`,
    USER_DATA_LOADING_ERROR = `USER_DATA_LOADING_ERROR`,    
    USER_DATA_CANCEL_LOADING = `CANCEL_LOADING`
}

export type AppAction =
    CancelSearchAction |
    LoadUserDataAction |
    LoadGeneralInfoAction |
    GeneralInfoHasBeenLoadedAction |
    GeneralInfoLoadingErrorAction |
    UserDataHasBeenLoadedAction |    
    UserDataLoadingErrorAction

export interface LoadUserDataAction extends Action {
    type: AppActionKeys.USER_DATA_LOAD
    payload: string
}
export const loadUserData = (payload: string): LoadUserDataAction => ({
    type: AppActionKeys.USER_DATA_LOAD,
    payload,    
})

export interface UserDataHasBeenLoadedAction extends Action {
    type: AppActionKeys.USER_DATA_HAS_BEEN_LOADED
    payload: any
}
export const userDataHasBeenLoaded = (payload: any): UserDataHasBeenLoadedAction => ({
    type: AppActionKeys.USER_DATA_HAS_BEEN_LOADED,
    payload,    
})

export interface CancelSearchAction extends Action {
    type:AppActionKeys.USER_DATA_CANCEL_LOADING
}
export const cancelSearch = (): CancelSearchAction => ({
    type: AppActionKeys.USER_DATA_CANCEL_LOADING,    
})

export interface UserDataLoadingErrorAction extends Action {
    type: AppActionKeys.USER_DATA_LOADING_ERROR
    error: any
}
export const userDataLoadingError = (error: any): UserDataLoadingErrorAction => ({
    type: AppActionKeys.USER_DATA_LOADING_ERROR,
    error,    
})

export interface LoadGeneralInfoAction extends Action {
    type: AppActionKeys.GENERAL_INFO_LOAD,
}
export const loadGeneralInfo = (): LoadGeneralInfoAction => ({
    type: AppActionKeys.GENERAL_INFO_LOAD
})

export interface GeneralInfoHasBeenLoadedAction extends Action {
    type: AppActionKeys.GENERAL_INFO_HAS_BEEN_LOADED,
    payload: GeneralInfo
}
export const generalInfoHasBeenLoaded = (payload: GeneralInfo): GeneralInfoHasBeenLoadedAction => ({
    type: AppActionKeys.GENERAL_INFO_HAS_BEEN_LOADED,
    payload
})
export interface GeneralInfoLoadingErrorAction extends Action {
    type: AppActionKeys.GENERAL_INFO_LOADING_ERROR
    error: any
}
export const generalInfoLoadingError = (error: any): GeneralInfoLoadingErrorAction => ({
    type: AppActionKeys.GENERAL_INFO_LOADING_ERROR,
    error,    
})