import { applyMiddleware, createStore, Store } from "redux"
import { createEpicMiddleware } from "redux-observable"

export const createSimpleStore = (rootReducer: any): Store => {
    return createStore(rootReducer)
}

export const createStoreWithEpicMiddleware = (rootReducer: any, rootEpic: any): Store => {
    const epicMiddleware: any = createEpicMiddleware()
    const store = createStore(rootReducer, applyMiddleware(epicMiddleware))
    epicMiddleware.run(rootEpic)
    return store
}

export const createStoreWithEpicMiddlewareAndDependencies = (rootReducer: any, rootEpic: any, dependencies: any): Store => {
    const epicMiddleware: any = createEpicMiddleware({
        dependencies: dependencies
    })
    const store = createStore(rootReducer, applyMiddleware(epicMiddleware))
    epicMiddleware.run(rootEpic)
    return store
}
  