import { Epic, ofType } from "redux-observable";
import { catchError, debounceTime, map, of, switchMap, takeUntil } from "rxjs";
import { CancelSearchAction, LoadUserDataAction, AppAction, AppActionKeys, userDataHasBeenLoaded, userDataLoadingError } from "../githubApiSandboxApp.actions";
import { AppState } from "../githubApiSandboxApp.reducer";
import { Dependencies } from "../githubApiSandboxApp.types";

interface QueryGithubUserEpic extends Epic<AppAction, AppAction, AppState> {}
export const queryGithubUserEpic: QueryGithubUserEpic = (action$, state$, dependencies: Dependencies) => {
  return action$.pipe(
    debounceTime(230, dependencies.scheduler),
    ofType<AppAction, AppActionKeys.USER_DATA_LOAD, LoadUserDataAction>(AppActionKeys.USER_DATA_LOAD),    
    switchMap((action: LoadUserDataAction) => {      
      return dependencies.getJSON(`https://api.github.com/users/${action.payload}`).pipe(
        map((userData: any) => userDataHasBeenLoaded(userData)),
        catchError((error) => of(userDataLoadingError(error)))
      )
    }),
    takeUntil(action$.pipe(
      ofType<AppAction, AppActionKeys.USER_DATA_CANCEL_LOADING, CancelSearchAction>(AppActionKeys.USER_DATA_CANCEL_LOADING)
    )),
  )
}