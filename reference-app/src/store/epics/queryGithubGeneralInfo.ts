import { Epic, ofType } from "redux-observable";
import { catchError, forkJoin, map, of, switchMap } from "rxjs";
import {
  generalInfoHasBeenLoaded,
  generalInfoLoadingError,
  LoadGeneralInfoAction,
  AppAction,
  AppActionKeys
} from "../githubApiSandboxApp.actions";
import { AppState } from "../githubApiSandboxApp.reducer";
import { Dependencies } from "../githubApiSandboxApp.types";

interface QueryGithubGeneralInfo extends Epic<AppAction, AppAction, AppState> {}
export const queryGithubGeneralInfo: QueryGithubGeneralInfo = (action$, state$, dependencies: Dependencies) => {
  return action$.pipe(    
    ofType<AppAction, AppActionKeys.GENERAL_INFO_LOAD, LoadGeneralInfoAction>(AppActionKeys.GENERAL_INFO_LOAD),    
    switchMap((action: LoadGeneralInfoAction) => {      
      return forkJoin({
        events: dependencies.getJSON<any[]>('https://api.github.com/events?per_page=1'),
        gists: dependencies.getJSON<any[]>('https://api.github.com/gists?per_page=1'),
      }).pipe(
        map((joinedResults) => {          
          return generalInfoHasBeenLoaded({
            latestGistAuthor: joinedResults.gists[0].owner.login,
            latestGistDesc: joinedResults.gists[0].description,
            latestGistUrl: joinedResults.gists[0].html_url,      
            latestEventRepoName: joinedResults.events[0].repo.name,
            latestEventType: joinedResults.events[0].type,                  
          })
        }),
        catchError((e) => of(generalInfoLoadingError(e)))
      )
    }),    
  )
}