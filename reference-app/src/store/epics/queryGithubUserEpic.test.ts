import { loadUserData } from "../githubApiSandboxApp.actions";
import { of } from "rxjs";
import { AppState, initialState } from "../githubApiSandboxApp.reducer";
import { Dependencies, getJSONType } from "../githubApiSandboxApp.types";
import { ajax } from "rxjs/ajax";
import { createTestScheduler, mockState } from "../../testUtils";
import { queryGithubUserEpic } from "./queryGithubUserEpic";


// should with rxjs 6 but doesnt work with rxjs7
function mockAjax$(result: any, error?: boolean) {
  const ajaxResponseMock = TypeMoq.Mock.ofType<AjaxResponse>()
  ajaxResponseMock.setup((mock) => mock.response).returns(() => result)
  const ajaxMock = TypeMoq.Mock.ofType<AjaxCreationMethod>()
  ajaxMock
    .setup((mock) => mock.post(TypeMoq.It.isAny(), TypeMoq.It.isAny(), TypeMoq.It.isAny()))
    .returns(() => (error ? Rx.throwError(result) : Rx.of(ajaxResponseMock.target)))
  return ajaxMock.target
}

const mockedGetJSON: <T> getJSONType = (url: string) => {    
  const emptyArray : T[] = []
  if (url === 'https://api.github.com/users/a') {
    return of([])
  } else {
    return of([])
  }
}

const testDependencies: Dependencies = {
  getJSON: ajax.getJSON,
  scheduler: createTestScheduler()
}

test('Gives back the right output action', () => {
  const input$ = of(loadUserData('a'))
  input$.subscribe(action => {    
    console.log(action.type)
  })  
  const mockedState = mockState<AppState>(initialState)
  const output$ = queryGithubUserEpic(input$, mockedState, testDependencies)
  output$.subscribe(action => {    
    console.log(action.type)    
  })
});
