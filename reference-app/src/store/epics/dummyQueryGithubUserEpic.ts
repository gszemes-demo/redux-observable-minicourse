import { Epic, ofType } from "redux-observable";
import { of, switchMap } from "rxjs";
import { LoadUserDataAction, AppAction, AppActionKeys, userDataHasBeenLoaded} from "../githubApiSandboxApp.actions";

interface DummyQueryGithubUserEpic extends Epic<AppAction> {}
export const dummyQueryGithubUserEpic: DummyQueryGithubUserEpic = (action$) => {
  return action$.pipe(    
    ofType<AppAction, AppActionKeys.USER_DATA_LOAD, LoadUserDataAction>(AppActionKeys.USER_DATA_LOAD),    
    switchMap((action: LoadUserDataAction) => {
      console.log('why not')    
      return (of(userDataHasBeenLoaded([])))      
    }),    
  )
}