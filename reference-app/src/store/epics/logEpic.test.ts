import { isEmpty, Observable, of } from "rxjs";
import { mockState } from "../../testUtils";
import { loadGeneralInfo, loadUserData } from "../githubApiSandboxApp.actions";
import { AppState, initialState } from "../githubApiSandboxApp.reducer";
import { logEpic } from "./logEpic";

const isObservableEmpty = (observable: Observable<any>) => {
    let isInputEmpty;  
    observable.pipe(isEmpty()).subscribe(
        (b: boolean) => {            
            isInputEmpty = b;
        }
    )
    return isInputEmpty;
}

test("Processes all actions, but the output streams are always empty", () => {
    const userDataLoad$ = of(loadUserData(''))
    const generalInfoLoad$ = of(loadGeneralInfo())

    const mockedState = mockState<AppState>(initialState)

    const output$ = logEpic(userDataLoad$, mockedState, undefined)    
    //expect(isObservableEmpty(output$)).toBe(true)
    expect(isObservableEmpty(output$)).toBe(false)
    
    const output2$ = logEpic(generalInfoLoad$, mockedState, undefined)    
    expect(isObservableEmpty(output2$)).toBe(true)
});
