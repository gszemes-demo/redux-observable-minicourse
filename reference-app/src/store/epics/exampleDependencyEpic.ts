import { Epic } from "redux-observable";
import { ignoreElements, tap } from "rxjs";
import { AppAction } from "../githubApiSandboxApp.actions";
import { AppState } from "../githubApiSandboxApp.reducer";

interface ExampleDependencyEpic extends Epic<AppAction, AppAction, AppState> {}
export const exampleDependencyEpic: ExampleDependencyEpic = (action$, state$, dependency) => {
  return action$.pipe(
    tap((action: AppAction) => {
      console.log(dependency)
    }),
    ignoreElements()
  )
}
