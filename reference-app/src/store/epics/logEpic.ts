import { Epic } from "redux-observable";
import { ignoreElements, tap } from "rxjs";
import { AppAction } from "../githubApiSandboxApp.actions";
import { AppState } from "../githubApiSandboxApp.reducer";

interface LogEpic extends Epic<AppAction, AppAction, AppState> {}
export const logEpic: LogEpic = (action$) => {
  return action$.pipe(
    tap((action: AppAction) => console.log("ACTION DISPATCHED: " + action.type)),
    ignoreElements()
  )
}