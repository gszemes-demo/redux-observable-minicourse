import React from 'react';
import { render, screen } from '@testing-library/react';
import {GithubApiSandboxApp} from './GithubApiSandboxApp';
import { createSimpleStore } from './store/createStore';
import { appReducer } from './store/githubApiSandboxApp.reducer';

test('renders learn react link', () => {
  render(<GithubApiSandboxApp />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
