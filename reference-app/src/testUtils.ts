import { StateObservable } from "redux-observable"
import { Subject } from "rxjs"
import { TestScheduler } from "rxjs/testing"

export function mockState<S = {}>(initialState: S = {} as S) {
    return new StateObservable<S>(new Subject<S>(), initialState)
}
  
export const createTestScheduler = () => {
    return new TestScheduler((actual, expected) => {
        return expect(actual).toEqual(expected)
    })
}