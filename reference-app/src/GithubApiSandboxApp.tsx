import {Store} from 'redux'
import {Provider as ReduxProvider} from 'react-redux'
import {HomePage} from './pages/HomePage'
import { appReducer } from './store/githubApiSandboxApp.reducer'
import { appEpic } from './store/githubApiSandboxApp.epic'
import {createStoreWithEpicMiddlewareAndDependencies } from './store/createStore'
import { ajax } from 'rxjs/ajax'
import {asyncScheduler} from 'rxjs'
import { Dependencies } from './store/githubApiSandboxApp.types'

export const GithubApiSandboxApp = () => {  
  const dependencies: Dependencies = {
    getJSON: ajax.getJSON,
    scheduler: asyncScheduler
  }
  const store = createStoreWithEpicMiddlewareAndDependencies(appReducer, appEpic, dependencies)  
  return (
    <ReduxProvider store={store}>
      <HomePage />
    </ReduxProvider>
  );
}