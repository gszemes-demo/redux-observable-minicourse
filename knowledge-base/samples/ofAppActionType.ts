import {Action} from 'redux'
import {OperatorFunction} from 'rxjs'
import {filter} from 'rxjs/operators'
import {Actions} from './Actions'

const keyHasType = (type: unknown, key: unknown) => {
  return type === key || (typeof key === 'function' && type === key.toString())
}

/**
 * Re-implementation of ofType of redux-observable because inferring does not work with
 * Typescript 4.5, rxjs 7.4 and redux-observable 2 as expected.
 *
 * Note: The problems seems to be the Input generic.
 */
export function ofAppActionType<
  // All possible actions your app can dispatch
  Input extends Actions,
  // The types you want to filter for
  Type extends Input['type'],
  // The resulting actions that match the above types
  Output extends Input = Extract<Input, Action<Type>>
>(...types: [Type, ...Type[]]): OperatorFunction<Input, Output> {
  const len = types.length
  return filter(
    len === 1
      ? (action): action is Output => keyHasType(action.type, types[0])
      : (action): action is Output => {
          for (let i = 0; i < len; i++) {
            if (keyHasType(action.type, types[i])) {
              return true
            }
          }
          return false
        }
  )
}
