import {CreateEpic, mockState$, testEpic} from '../_utils/epicTestUtils'
import {DebounceUserInputActions, sendUserInput, search} from './debounceUserInput.actions'
import {debounceUserInput$} from './debounceUserInput.epics'

describe('debounceUserInput$', () => {
  test('check that the input is debounced', () => {
    const inputActions = {
      a: sendUserInput('a'),
      b: sendUserInput('ab'),
      c: sendUserInput('abc'),
    }
    const outputActions = {
      a: search('abc'),
    }
    const createEpic: CreateEpic<DebounceUserInputActions> = (action$, scheduler) => {
      return debounceUserInput$(action$, mockState$({}), {scheduler})
    }
    testEpic(createEpic, inputActions, outputActions, '-abc', '- -- 230ms a')
  })

  test('test that only changed input values trigger the output action', () => {
    const inputActions = {
      a: sendUserInput('ab'),
      b: sendUserInput('ab'),
      c: sendUserInput('abc'),
    }
    const outputActions = {
      a: search('ab'),
      b: search('abc'),
    }
    const createEpic: CreateEpic<DebounceUserInputActions> = (action$, scheduler) => {
      return debounceUserInput$(action$, mockState$({}), {scheduler})
    }
    testEpic(createEpic, inputActions, outputActions, '- a 230ms b c', '- 230ms a 230ms - b')
  })
})
