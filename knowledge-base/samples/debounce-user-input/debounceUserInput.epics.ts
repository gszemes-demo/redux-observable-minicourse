import {Epic} from 'redux-observable'
import {debounceTime, distinctUntilChanged, map, SchedulerLike} from 'rxjs'
import {ofAppActionType} from '../ofAppActionType'
import {DebounceUserInputActionKeys, DebounceUserInputActions, search} from './debounceUserInput.actions'

export interface Services {
  scheduler: SchedulerLike
}

export interface DebounceUserInputEpic extends Epic<DebounceUserInputActions, DebounceUserInputActions, {}, Services> {}

export const debounceUserInput$: DebounceUserInputEpic = (action$, state$, {scheduler}) => {
  return action$.pipe(
    ofAppActionType(DebounceUserInputActionKeys.SEND_USER_INPUT),
    // Alternative 1: use the filter operator
    // filter<SendUserInputAction>( (a) => a.type === DebounceUserInputActionKeys.SEND_USER_INPUT),
    // Alternative 2: use the ofType operator of redux-observable
    // ofType<DebounceUserInputActions, DebounceUserInputActionKeys.SEND_USER_INPUT, SendUserInputAction>(DebounceUserInputActionKeys.SEND_USER_INPUT),
    debounceTime(230, scheduler),
    distinctUntilChanged((p, c) => p.input === c.input),
    map((action) => search(action.input))
  )
}
