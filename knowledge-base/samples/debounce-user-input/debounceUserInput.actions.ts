import {Action} from 'redux'

export enum DebounceUserInputActionKeys {
  SEND_USER_INPUT = 'debounceUserInput/SEND_USER_INPUT',
  SEARCH = 'debounceUserInput/SEARCH',
}

export type DebounceUserInputActions = SendUserInputAction | SearchAction

export interface SendUserInputAction extends Action {
  type: DebounceUserInputActionKeys.SEND_USER_INPUT
  input: string
}

export const sendUserInput = (input: string): SendUserInputAction => ({
  type: DebounceUserInputActionKeys.SEND_USER_INPUT,
  input,
})

export interface SearchAction extends Action {
  type: DebounceUserInputActionKeys.SEARCH
  query: string
}

export const search = (query: string): SearchAction => ({
  type: DebounceUserInputActionKeys.SEARCH,
  query,
})
