import {DebounceUserInputActions} from './debounce-user-input/debounceUserInput.actions'

export type Actions = DebounceUserInputActions
