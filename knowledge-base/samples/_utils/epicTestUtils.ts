import {Action} from 'redux'
import {StateObservable} from 'redux-observable'
import * as Rx from 'rxjs'
import {TestScheduler} from 'rxjs/testing'

// see https://github.com/ReactiveX/rxjs/blob/master/docs_app/content/guide/testing/marble-testing.md
// see https://www.nexthink.com/blog/marble-testing-redux-observable-epics/
// see https://github.com/florinn/typemoq#create-mocks

export type CreateEpic<A extends Action> = (action: Rx.Observable<A>, scheduler: Rx.SchedulerLike) => Rx.Observable<any>

export function testEpic(
  createEpic: CreateEpic<any>,
  inputActions: any,
  outputActions: any,
  inputMarble: string,
  outputMarble: string
) {
  // Create test scheduler
  const testScheduler = createTestScheduler()

  // Run the tests in a virtualized environment
  testScheduler.run(({expectObservable}) => {
    // Assert on the resulting actions observable
    const action$ = createTestAction$FromMarbles<Action>(testScheduler, inputMarble, inputActions)

    // Apply epic on actions observable
    const outputAction$ = createEpic(action$, testScheduler)

    // Assert on the resulting actions observable
    expectObservable(outputAction$).toBe(outputMarble, outputActions)
  })
}

export function testSimpleEpic<Input extends Action, Output>(
  createEpic: CreateEpic<any>,
  inputAction: Input,
  outputAction: Output
) {
  const inputActions = {
    a: inputAction,
  }
  const outputActions = {
    a: outputAction,
  }
  testEpic(createEpic, inputActions, outputActions, '-a', '-a')
}

export function mockState$<S = {}>(initialState: S = {} as S) {
  return new StateObservable<S>(new Rx.Subject<S>(), initialState)
}

const createTestScheduler = () => {
  return new TestScheduler((actual, expected) => {
    return expect(actual).toEqual(expected)
  })
}

function createTestAction$FromMarbles<A extends Action>(testScheduler: TestScheduler, marbles: string, values?: any) {
  return testScheduler.createHotObservable(marbles, values)
}
