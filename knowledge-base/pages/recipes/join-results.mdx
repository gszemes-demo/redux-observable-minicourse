import Callout from 'nextra-theme-docs/callout'
import Image from 'next/image'

# Join the results of multiple observables

You'll learn how to join multiple action streams and return only one.

## The problem

You have a react-redux app with redux-observables configured.
As a sideffect you want to _asynchronously_ make multiple
ajax requests, and want to be able to process the resulting observables.
You're at an Epic, so you must return only 1 observable.

## The solution

This usecase calls for the use of ```forkJoin```:

```typescript
interface ExampleForkJoinEpic // ...
export const exampleForkJoinEpic$: ExampleForkJoinEpic = (action$) => {
  return action$.pipe(
    // ...  
    switchMap(() => {      
      return Rx.forkJoin({
        someResult: ajax.getJSON('/some/url'),
        otherResult: ajax.getJSON('/some/other/url'),
      }).pipe(
        map((joinedResults) => {
          return someSuccessAction(joinedResults.someResult, joinedResults.otherResult)
        }),
        catchError((e) =>
            // ...
        )
      )
    })
  )
}
```

With ```forkJoin``` we can 'handle' any number of requests (observables), as theye were only one.
Once all the joined observables are finished, a result object will be constructed out of all the
results. 

## Workshop

To demonstrate the usage of ```forkJoin```, lets display some information about random recent
activities, that we will query from 2 different github APIs (```/events``` and ```/gists```).
Let's create separate chapters also. After completing the workshop your application should
look something like this:  

<div style={{marginTop: "12px"}}>
    <Image src="/join-results-image-1.png" width={601} height={250} borderRadius="5" />
</div>

<Callout >
   A 'gist' is (by gitHub's definition): An instantly shareable code, note or snippet.
</Callout>

Lets create a type first, a simple type that holds all all the data we'll need from the
endpoints as strings. We will store this type at a separate file:

📜 searchApp.types.ts (new file)
```typescript
export interface GeneralInfo {
    latestGistAuthor: string,
    latestGistDesc: string,
    latestGistUrl: string,       
    latestEventType: string,
    latestEventRepoName: string
}
```


Now, we can create some actions. This time the queries will take only a little time,
so a loading state is not necessary, but an error action is advisable for later use.
So 3 actions, a 'loading', a 'has-been-loaded' and an 'error':


📜 searchApp.actions.ts (new lines)
```typescript
export enum SearchAppActionKeys {
    // ...
    GENERAL_INFO_HAS_BEEN_LOADED =  `GENERAL_INFO_HAS_BEEN_LOADED`,
    GENERAL_INFO_LOADING_ERROR = `GENERAL_INFO_LOADING_ERROR`,
    GENERAL_INFO_LOAD = `GENERAL_INFO_LOAD`
}
export type SearchAppAction =
    // ...
    LoadGeneralInfoAction |
    GeneralInfoHasBeenLoadedAction |
    GeneralInfoLoadingErrorAction   
    export interface LoadGeneralInfoAction extends Action {
    type: SearchAppActionKeys.GENERAL_INFO_LOAD,
}
export const loadGeneralInfo = (): LoadGeneralInfoAction => ({
    type: SearchAppActionKeys.GENERAL_INFO_LOAD
})

export interface GeneralInfoHasBeenLoadedAction extends Action {
    type: SearchAppActionKeys.GENERAL_INFO_HAS_BEEN_LOADED,
    payload: GeneralInfo
}
export const generalInfoHasBeenLoaded = (payload: GeneralInfo): GeneralInfoHasBeenLoadedAction => ({
    type: SearchAppActionKeys.GENERAL_INFO_HAS_BEEN_LOADED,
    payload
})
export interface GeneralInfoLoadingErrorAction extends Action {
    type: SearchAppActionKeys.GENERAL_INFO_LOADING_ERROR
    error: any
}
export const generalInfoLoadingError = (error: any): GeneralInfoLoadingErrorAction => ({
    type: SearchAppActionKeys.GENERAL_INFO_LOADING_ERROR,
    error,    
})  
```

Once we have our actions, we can concentrate to our epic. This example will demonstrate
how to join the results of 2 different API calls with ```forkJoin```, and how to map the
results afterwards. We only query 1 answer per page, and only the 1 page, of both endpoints.

📜 queryGithubGeneralInfo.tsx (new file)
```typescript
import { Epic, ofType } from "redux-observable";
import { catchError, forkJoin, map, of, switchMap } from "rxjs";
import {
  generalInfoHasBeenLoaded,
  generalInfoLoadingError,
  LoadGeneralInfoAction,
  AppAction,
  AppActionKeys
} from "../githubApiSandboxApp.actions";
import { AppState } from "../githubApiSandboxApp.reducer";
import { Dependencies } from "../githubApiSandboxApp.types";

interface QueryGithubGeneralInfo extends Epic<SearchAppAction, SearchAppAction, SearchAppState> {}
export const queryGithubGeneralInfo: QueryGithubGeneralInfo = (action$, state$, dependencies: Dependencies) => {
  return action$.pipe(    
    ofType<SearchAppAction, SearchAppActionKeys.GENERAL_INFO_LOAD, LoadGeneralInfoAction>(SearchAppActionKeys.GENERAL_INFO_LOAD),    
    switchMap((action: LoadGeneralInfoAction) => {      
      return forkJoin({
        events: dependencies.getJSON<any[]>('https://api.github.com/events?per_page=1'),
        gists: dependencies.getJSON<any[]>('https://api.github.com/gists?per_page=1'),
      }).pipe(
        map((joinedResults) => {
          return generalInfoHasBeenLoaded({
            latestGistAuthor: joinedResults.gists[0].owner.login,
            latestGistDesc: joinedResults.gists[0].description,
            latestGistUrl: joinedResults.gists[0].html_url,      
            latestEventRepoName: joinedResults.events[0].repo.name,
            latestEventType: joinedResults.events[0].type,                  
          })
        }),
        catchError((e) => of(generalInfoLoadingError(e)))
      )
    }),    
  )
}
```

Don't forget to include this epic into  ```combine``` at the main epic, otherwise it will not take effect:

📜 githubApiSandboxApp.epic.ts (new lines)
```typescript
export const searchAppEpic = combineEpics(
    // ...
    queryGithubGeneralInfo    
)
```

Let's look at the state/error handling at the reducer. We won't handle a loading state here, so only an 'error'
(```eneralInfoLoadingError```) and a 'result' (```generalInfo```) variable is added to the state.

<Callout >
   As we want to set different (and disjunct) partial states, we'll have to intrude
   state deconstruction (```...state```) everywhere.
</Callout>


📜 searchApp.reducer.tsx (new lines)
```typescript
export interface SearchAppState {
    // ...
    generalInfo?: GeneralInfo
    eneralInfoLoadingError?: any  
}
export const appReducer = (state: AppState = initialState, action: AppAction): Partial<AppState>  => {
    switch (action.type) {
        case AppActionKeys.USER_DATA_CANCEL_LOADING:
            return {         
                ...state,       
                userData: undefined,
                isUserDataLoading: false
            }
        case AppActionKeys.USER_DATA_HAS_BEEN_LOADED:
            return {
                ...state,
                userData: action.payload,
                isUserDataLoading: false
            }
        case AppActionKeys.USER_DATA_LOAD:
            return {
                ...state,
                searchInputValue: action.payload,
                isUserDataLoading: true
            }
        case AppActionKeys.USER_DATA_LOADING_ERROR:
            return {
                ...state,
                error: action.error,
                isUserDataLoading: false
            }
        case AppActionKeys.GENERAL_INFO_HAS_BEEN_LOADED:
            return {     
                ...state,           
                generalInfo: action.payload
            }
        case AppActionKeys.GENERAL_INFO_LOADING_ERROR:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}
```

We will trigger the loading action once the HomePage is loaded.
Lets see how we restructured the site:

📜 HomePage.tsx (new lines)
```typescript

export const HomePage = () => {      
  // ...    
  const generalInfo = useSelector((state: SearchAppState) => state.generalInfo)    

  // ...
  useEffect(() => {    
    dispatch(loadGeneralInfo())
  }, [])

  return (    
    <div >  
      <h2>Github sandbox</h2>
      <h3>Recent Activities</h3> 
      {generalInfo &&
        <>
          <div>
            <b>Latest gist:</b> <a href={generalInfo.latestGistUrl}>{generalInfo.latestGistDesc || '[no description]'}</a>
            &nbsp;(by {generalInfo.latestGistAuthor}).
          </div>          
          <div>
            <b>Latest event:</b> a <i>{generalInfo.latestEventType}</i>
            &nbsp;at repository <a href={`https://github.com/${generalInfo.latestEventRepoName}`}>{generalInfo.latestEventRepoName}</a>.
          </div>          
        </>
      }
      <h3>User finder</h3>
      {/* ... (the already implemented user search utility goes here) */}
    </div>
  );
}
}